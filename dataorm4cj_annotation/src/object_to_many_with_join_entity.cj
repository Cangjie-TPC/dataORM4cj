/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */
macro package dataorm4cj_annotation

class ToManyWithJoinEntityObj <: ToManyBaseObj {
    let joinEntity: EntityObj
    let sourceProperty: PropertyObj
    let targetProperty: PropertyObj

    public init(schema: SchemaObj, sourceEntity: EntityObj, targetEntity: EntityObj, joinEntity: EntityObj,
            sourceProperty: PropertyObj,
            targetProperty: PropertyObj) {
        super(schema, sourceEntity, targetEntity)
        this.joinEntity = joinEntity
        this.sourceProperty = sourceProperty
        this.targetProperty = targetProperty
    }

    public func getSourceProperties(): PropertyObj {
        return sourceProperty
    }

    public func getJoinEntity(): EntityObj {
        return joinEntity
    }

    public func getTargetProperty(): PropertyObj  {
        return targetProperty
    }

    public func init3rdPass() {
        super.init3rdPass()
        let pks = this.sourceEntity.getOrThrow().getPropertiesPk()
        if (pks.isEmpty()) {
            throw DaoGeneratorException("Source entity has no primary key, but we need it for ${this}")
        }
        let pks2 = targetEntity.getOrThrow().getPropertiesPk()
        if (pks2.isEmpty()) {
            throw DaoGeneratorException("Target entity has no primary key, but we need it for ${this}")
        }
    }
}