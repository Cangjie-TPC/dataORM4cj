<div align="center">
<h1>dataORM4cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.53.18-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-0%25-red" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## 介绍

dataORM4cj 是一个基于ORM的仓颉版终端sqlite数据库框架, 用于简化本地数据库的操作。提供了高效的数据库访问性能和低内存消耗。dataORM4cj 支持多线程操作、链式调用、缓存等特性等功能。其设计理念是轻量、快速且易于使用，帮助开发者快速构建高性能的应用程序。

### 效果展示
![效果展示](doc/gif/sample.gif)

### 特性

- 支持实体类自动生成空参构造函数, 全参构造函数, getter函数, setter函数
- 支持注解开发
- 支持标准数据库套件和加密套件
- 支持Long，object包装类型
- 支持查询，删除，修改，新增功能
- 支持事务功能
- 支持索引注解 @Index, @Unique
- 支持基础属性注解 @Entity, @Id，@NotNull，@Transient, @Property, @OrderBy
- 支持关系型注解 @ToOne, @ToMany, @JoinEntity
- 支持转换器注解 @PropertyConverter, @Convert
- 支持DaoMaster，DaoSession，实体Dao实现类生成


## 软件架构

### 源码目录

```shell
├─AppScope
├─doc                         // 接口文档
├─entry                       // UI模块 
├─dataorm4cj                  // 核心代码模块      
├─dataorm4cj_annotation       // 注解生成器模块
└─hvigor                      // 构建工具目录

```


### 接口说明

主要类和函数接口说明详见 [API](doc/API.md)

## 使用说明

### 编译运行

    使用DevEco Studio搭配com.huawei.cangjie-support-plugin插件用于
    构建工程项目, 生成 hap 包.

### 功能示例

用例代码在entry目录下 [功能示例](entry/src/main/cangjie/src/index.cj)

## 约束与限制
1. 在下述版本验证通过：

- DevEco Studio: 5.0.2 Release
- Cangjie support Plugin: 5.0.7.100

2. 实体类约束:
- @Entity修饰的实体类成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array\<Byte\>/Bool/DateTime
- 如需成员变量支持其他类型, 请参考[转换器功能](doc/API.md#112-propertyconverter-和-convert)使用教程


3. dataorm4cj.cfg文件配置:
- 需要先在仓颉实体类项目的根路径配置 dataorm4cj.cfg 配置文件, 例如[点这里](entry/src/main/cangjie/dataorm4cj.cfg)
- 如果找不到配置路径. 第一次编译会自动生成dataorm4cj.cfg.example文件, 请把 dataorm4cj.cfg.example文件重命名为dataorm4cj.cfg即可修改.
```json
{
  "schemaVersion": 1,
  "daoPackage": "实体类的包名",
  "targetGenDir": "实体类相对于自动生成的dataorm4cj.cfg.example文件的相对路径",
  "logLevel": "INFO"
}
```

4. entry用例使用提示:
- 使用时先将以下仓颉标准库的so拷贝到 (x86_64环境)entry/libs/x86_64、 (arm64环境)entry/libs/arm64-v8a目录下， 目录不存在需要创建: (此类情况是暂时性,相关问题单已提至ohos支撑开发)
- libcangjie-std-ast.so
- libcangjie-std-collection.so
- libcangjie-std-core.so
- libcangjie-std-math.so
- libcangjie-std-sort.so

5. 场景使用约束：
```cangjie
@Entity
class Demo {
    @Id
    var id: Int64
    var name: String
}
DemoDao.properties["name"] //使用DemoDao.properties时需要传入和Demo类对应成员变量名, 传入的字符串对应变量不存在时(区分大小写), 会抛NoneValueException.
```

## 开源协议

本项目基于 [Apache License 2.0](/LICENSE) ，项目greendao_annotation基于 [GPL license 3.0](/LICENSE) ，请自由的享受和参与开源。

## 参与贡献

欢迎给我们提交 PR，欢迎给我们提交 issue，欢迎参与任何形式的贡献。