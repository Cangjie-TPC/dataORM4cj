/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights resvered.
 */
package ohos_app_cangjie_entry

import ohos.base.*
import ohos.ability.*
import ohos.window.*
import ohos.component.*
import ohos.state_manage.*
import ohos.state_macro_manage.*
import ohos.router.*
import ohos.prompt_action.PromptAction
import ohos.relational_store.*
import ohos.state_macro_manage.Entry
import ohos.state_macro_manage.Component
import ohos.state_macro_manage.State
import ohos.state_macro_manage.r

var userDao: UserDao = unsafe { zeroValue<UserDao>() }
var teacherDao: TeacherDao = unsafe { zeroValue<TeacherDao>() }
var personDao: PersonDao = unsafe { zeroValue<PersonDao>() }
var daoSession: DaoSession = unsafe { zeroValue<DaoSession>() }
var toManyEntityDao: ToManyEntityDao = unsafe { zeroValue<ToManyEntityDao>() }
var dateEntityDao: DateEntityDao = unsafe { zeroValue<DateEntityDao>() }
var joinManyToDateEntityDao: JoinManyToDateEntityDao = unsafe { zeroValue<JoinManyToDateEntityDao>() }
@Entry
@Component
class MyView {

    protected func onAppear() {
        let db = getRdbStore(getStageContext(getGlobalAbilityContext()), StoreConfig("RdbTest.db", SecurityLevel.S1))
        var daoMaster: DaoMaster = DaoMaster(db)
        DaoMaster.dropAllTables(daoMaster,true)
        DaoMaster.createAllTables(daoMaster, true)
        daoSession = daoMaster.newSession()
        userDao = daoSession.getUserDao()
        teacherDao = daoSession.getTeacherDao()
        personDao = daoSession.getPersonDao()
        toManyEntityDao = daoSession.getToManyEntityDao()
        dateEntityDao = daoSession.getDateEntityDao()
        joinManyToDateEntityDao = daoSession.getJoinManyToDateEntityDao()
        IQueryBuilder.LOG_SQL = true
        let arr = [
                User(Option<Int64>.None, 1, "lisi", "111"),
                User(Option<Int64>.None, 2, "zhangsan", "222"),
                User(Option<Int64>.None, 3, "wangwu", "333"),
                User(Option<Int64>.None, 4, "zhaoliu", "444"),
                User(Option<Int64>.None, 5, "tianqi", "555")
            ]
        for (item in arr) {
            userDao.insertOrReplace(item)
        }

        let arr2 = [
                Teacher(Option<Int64>.None,"zhang san", "西安市雁塔区科技二路", 28,165,110,96.326),
                Teacher(Option<Int64>.None,"li四si", "西安市雁塔区科技二路", 28,163,110,98.309),
                Teacher(Option<Int64>.None,"王腾", "西安市雁塔区科技五路", 29,185,170,94.46)
            ]
        for (item in arr2) {
            teacherDao.insertOrReplace(item)
        }

        let arr3 = [
                ToManyEntity(1, "a"),
                ToManyEntity(2, "b"),
                ToManyEntity(3, "c"),
                ToManyEntity(4, "d")
            ]
        let arr4 = [
                DateEntity(1, 12345678),
                DateEntity(2, 89132456),
                DateEntity(3, 666666),
                DateEntity(4, 999999)
            ]
        let arr5 = [
                JoinManyToDateEntity(Option<Int64>.None, 1, 2),
                JoinManyToDateEntity(Option<Int64>.None, 2, 1),
                JoinManyToDateEntity(Option<Int64>.None, 3, 4),
                JoinManyToDateEntity(Option<Int64>.None, 4, 3)
            ]
        for (item in arr3) {
            toManyEntityDao.insertOrReplace(item)
        }
        for (item in arr4) {
            dateEntityDao.insertOrReplace(item)
        }
        for (item in arr5) {
            joinManyToDateEntityDao.insertOrReplace(item)
        }
    }

    let scroller = Scroller()

    func build() {
        // Row {
            Scroll(this.scroller){
                Column {
                    Text("--Greendao 简单用例--").height(80)

                    Divider().color(0x000000).strokeWidth(1).margin(top: 10, bottom: 10)

                    Row() {
                        Button("查询").onClick {
                            evt => Router.push(url: "PageQuery")
                        }.fontSize(20).height(40)

                        Button("新增").onClick {
                            evt => Router.push(url: "PageAdd")
                        }.fontSize(20).height(40)
                    }

                    Text("--Greendao HLT用例--").height(80)

                    Divider().color(0x000000).strokeWidth(1).margin(top: 10, bottom: 10)

                    Button("testToone")
                        .height(40)
                        .margin(top: 5, bottom: 5)
                        .fontSize(16)
                        .width(90.percent)
                        .shape(ShapeType.Capsule)
                        .onClick { =>
                            //testToOne()
                        }
                    Button("testOneToOne")
                        .height(40)
                        .margin(top: 5, bottom: 5)
                        .fontSize(16)
                        .width(90.percent)
                        .shape(ShapeType.Capsule)
                        .onClick { =>
//                            testOneToOne()
                        }
                    Button("testToMany")
                        .height(40)
                        .margin(top: 5, bottom: 5)
                        .fontSize(16)
                        .width(90.percent)
                        .shape(ShapeType.Capsule)
                        .onClick { =>
                            testToMany()
                        }

                    Button("testJoin")
                        .height(40)
                        .margin(top: 5, bottom: 5)
                        .fontSize(16)
                        .width(90.percent)
                        .shape(ShapeType.Capsule)
                        .onClick { =>
                            testJoin()
                        }

                    //HLT
                    // Button("DeleteTest01").onClick {Router.push(url: "DeleteTest01")}
                    // Button("AddTest01").onClick {Router.push(url: "AddTest01")}
                    // Button("AddTest02").onClick {Router.push(url: "AddTest02")}
                    // Button("QueryTest01").onClick {Router.push(url: "QueryTest01")}
                    // Button("QueryTest02").onClick {Router.push(url: "QueryTest02")}
                    // Button("QueryTest03").onClick {Router.push(url: "QueryTest03")}
                    // Button("UpdateTest01").onClick {Router.push(url: "UpdateTest01")}

                }.width(100.percent).margin(top: 5)
            }.onAppear({ =>
                onAppear()
            })
        // }

    }
}


func testToMany(): Unit {
    let scoreDao: ScoreDao = daoSession.getScoreDao()
    let studentDao: StudentDao = daoSession.getStudentDao()
    let math =  Score("1101", 87, "Math", "110");
    let english =  Score("1102", 99, "English", "110");
    let chinese =  Score("1103", 120, "Chinese", "110");
    scoreDao.insertOrReplaceInTx(math,english,chinese);//使用事务插入或替换数据
    let magicer: Student =  Student("110", "Magicer", 23);
    studentDao.insertOrReplace(magicer);
    let arr = studentDao.queryBuilder().`where`(StudentDao.properties["name"].eq("Magicer")).build().list();
//    let list = GreenDaoQueryBuilder(scoreDao).`where`(ScoreDao.properties["studentId"].eq("110")).build().list < Score >()
//    PromptAction.showToast(message: "------------------- list.size() = ${list.size}")
    for (a in arr) {
        let gs = a.getScore()
        PromptAction.showToast(message: "------------------- gs.size() = ${gs?.size}")
    }
}

//func testToOne(): Unit {
//    let scoreDao: ScoreDao = daoSession.getScoreDao()
//    let studentDao: StudentDao = daoSession.getStudentDao()
//    //先向数据库中插入两条数据
//    scoreDao.insertOrReplace(Score("1101", 80))
//    studentDao.insertOrReplace(Student("110","Magicer",12,"1101"))
//    let querys = studentDao.queryBuilder().`where`(
//        StudentDao.properties["name"].eq("Magicer")
//    ).build().list<Student>();
//
//    let ss = querys[0].getScore()
//    if (let Some(v) <- ss) {
//        PromptAction.showToast(message: "------------------- v.getScore() = ${v.getScore()}")
//    } else {
//        PromptAction.showToast(message: "------------------- v.getScore() = none")
//    }
//}


//func testOneToOne(): Unit {
//    let scoreDao: ScoreDao = daoSession.getScoreDao()
//    let studentDao: StudentDao = daoSession.getStudentDao()
//
//
//    let  stu = Student()
//    stu.setId("101")
//    stu.setName("123")
//    stu.setAge(1000)
//
//    let sco = Score()
//    sco.setId("999")
//    sco.setScore(100)
//
//    studentDao.insert(stu)
//    sco.setStudent(stu)
//    scoreDao.insert(sco)
//    stu.setScore(sco)
//    studentDao.update(stu)
//
//    let sss = studentDao.queryBuilder().`where`(
//        StudentDao.properties["name"].eq("123")
//    ).build().unique<Student>();
//
//    let vvv = sss.getOrThrow().getId()
//    PromptAction.showToast(message: "------------------- vvv = ${vvv}")
//    let ss = sss?.getScore()
//    if (let Some(v: Score) <- ss) {
//        PromptAction.showToast(message: "------------------- v.getScore() = ${v.getScore()}")
//    } else {
//        PromptAction.showToast(message: "------------------- v.getScore() = none")
//    }
//}

func testJoin(): Unit {
//        toManyEntityDao = daoSession.getToManyEntityDao()
//        dateEntityDao = daoSession.getDateEntityDao()
//        joinManyToDateEntityDao = daoSession.getJoinManyToDateEntityDao()
    //先向数据库中插入两条数据
   let build = toManyEntityDao.queryBuilder()
    build.join<DateEntity>(toManyEntityDao.PROPERTIES["id"], JoinManyToDateEntity.CLASS(), joinManyToDateEntityDao.PROPERTIES["id"])
    .`where`(joinManyToDateEntityDao.PROPERTIES["id"].eqSql(2))
    let arr = build.buildSql().list()
    PromptAction.showToast(message: "------------------- v.getScore() = ${arr.size}")
}

