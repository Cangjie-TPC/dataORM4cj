# 1.0.1
- 升级仓颉版本 0.53.13
- 适配arm64-v8a编译

# 1.0.0

- 支持实体类自动生成空参构造函数, 全参构造函数, getter函数, setter函数
- 支持注解开发
- 支持标准数据库套件和加密套件
- 支持Long，object包装类型
- 支持查询，删除，修改，新增功能
- 支持事务功能
- 支持索引注解 @Index, @Unique
- 支持基础属性注解 @Entity, @Id，@NotNull，@Transient, @Property, @OrderBy
- 支持关系型注解 @ToOne, @ToMany, @JoinEntity
- 支持转换器注解 @PropertyConverter, @Convert
- 支持DaoMaster，DaoSession，实体Dao实现类生成
- 支持一对一, 一对多, 多对多查询
- 