/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

/**
 * Created on 2024/4/16
 */
package dataorm4cj

sealed abstract class AbstractQueryData<T, Q> where Q <: AbstractQuery<T> , T <: EntityType{
    let sql: sqlType
    protected let dao: IAbstractDao
    protected let initialValues: Array<ValueType>
    protected let queriesForThreads: Map<Int64, Q>
    protected let lock = ReentrantMutex()

    init(dao: IAbstractDao, sql: sqlType, initialValues: Array<ValueType>) {
        this.dao = dao
        this.sql = sql
        this.initialValues = initialValues
        queriesForThreads = HashMap<Int64, Q>()
    }
    
    /**
     * Just an optimized version, which performs faster if the current thread is already the query's owner thread.
     * Note: all parameters are reset to their initial values specified in {@link QueryBuilder}.
     */
    protected func forCurrentThread(query: Q): Q {
        if (Thread.currentThread.id == query.ownerThread.id) {
            // System.arraycopy(initialValues, 0, query.parameters, 0, initialValues.length)
            initialValues.copyTo(query.parameters, 0, 0, initialValues.size)
            return query
        } else {
            return forCurrentThread()
        }
    }

    /**
     * Note: all parameters are reset to their initial values specified in {@link QueryBuilder}.
     */
    func forCurrentThread(): Q {
        // Process.myTid() seems to have issues on some devices (see Github #376) and Robolectric (#171):
        // We use currentThread().getId() instead (unfortunately return a long, can not use SparseArray).
        // PS.: thread ID may be reused, which should be fine because old thread will be gone anyway.
        var threadId = Thread.currentThread.id
        synchronized (lock) {
            return if (let Some(v) <- queriesForThreads.get(threadId)) {
                initialValues.copyTo(v.parameters, 0, 0, initialValues.size)
                v
            } else {
                // gc() 
                let query = createQuery()
                queriesForThreads.put(threadId, query)
                query
            } 
        }
    }

    protected func createQuery(): Q

    // TODO func gc WeakReference
}