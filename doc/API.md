# dataORM4cj API

## 1. 注解

    需要先在仓颉项目根路径配置 dataorm4cj.cfg 文件
    dataorm4cj.cfg 配置例子
```json
{
  "schemaVersion": 1,
  "daoPackage": "实体类的包名",
  "targetGenDir": "实体类相对于自动生成的dataorm4cj.cfg.example文件的相对路径",
  "logLevel": "INFO"
}
```

### 1.1 @Entity
```cangjie

/**
 * 修饰实体类的宏, 表示实体类会在数据库中生成一个与之相对应的表
 * 参数 nameInDb - 指定此实体映射到的DB端的名称（例如表名）。默认情况下，该名称基于实体类名。
 * 参数 createInDb - 高级标志，用于禁用数据库中的表创建（设置为false时）。这可用于创建局部实体，这些实体可能仅使用特性的子集。但是，请注意，greenDAO不会同步多个实体，例如缓存中的实体。
 * 参数 schema - 指定实体的架构名称：greenDAO可以为每个架构生成独立的类集。属于不同模式的实体不应具有关系。
 * 参数 active - 是否应生成更新/删除/刷新方法。若实体定义了ToMany或ToOne关系，则它独立于该值处于活动状态，不应具有关系。
 * 参数 generateConstructors - 是否应生成所有属性构造函数, 总是需要无参数构造函数。
 * 参数 generateGettersSetters - 是否应生成所有属性的getter和setter，是否应生成它们。                     
 * 该注解的各个参数之间没有顺序                  
 */
@Entity[
    "nameInDb" = String, 
    "createInDb" = Bool, 
    "schema" = String, 
    "active" = Bool, 
    "generateConstructors" = Bool, 
    "generateGettersSetters" = Bool
    ]

/**
 * 修饰实体类的宏, 表示实体类会在数据库中生成一个与之相对应的表
 */
@Entity
```
#### 使用方法
```cangjie
// 示例1
@Entity
public class Demo {
    public var id: Int64
}
// 示例2
@Entity[nameInDb="AnnotationBean",createInDb=true,schema="default",active=true,generateConstructors=true,generateGettersSetters=true]
public class Demo{
    public var id: Int64
}
```
### 1.2 @Transient 
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示当前字段不会存入到数据库表中, 若实体类中所有成员变量都有@Transient修饰, 则不会创建该实体类对应数据表。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 */
@Transient

```
#### 使用方法
```cangjie
@Entity
public class Demo {
    @Transient
    public var id: Int64
}
```

### 1.3 @Index
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示当前字段在数据库表中创建的索引，该注解无法和@Unique注解同时使用，共同使用会抛IllegalStateException。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 */
@Index

/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示当前字段在数据库表中创建的索引，该注解无法和@Unique注解同时使用，共同使用会抛IllegalStateException。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 * 参数 value - 指定顺序, ASC或者DESC（参数不区分大小写）
 * 参数 name - 索引的可选名称。如果省略，则由dataORM4cj的注解器自动生成
 * 参数 unique - 是否应基于此索引创建唯一约束
 * 该注解的各个参数之间没有顺序
 */
@Index[
    "value" = String,
    "name" = String,
    "unique" = Bool
    ]
```
#### 使用方法
```cangjie
@Entity
public class Demo {
    @Index
    public var id: Int64
}
```
### 1.4 @Unique
```cangjie

/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示当前字段在数据库表中是唯一的属性, 该注解无法和@Index注解共同使用，共同使用会抛IllegalStateException。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 */
@Unique

```
#### 使用方法
```cangjie
@Entity
public class Demo {
    @Unique
    public var id: Int64
}
```

### 1.5 @NotNull
```cangjie

/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示当前字段在数据表中不能为空（Option<T>.None）。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 */
@NotNull
```
#### 使用方法
```cangjie
@Entity
public class Demo {
    @NotNull
    public var id: Int64
}
```
### 1.6 @Id
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示实体类在数据库表的主键ID, 若autoincrement=true时, 类型必须为整数。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 * 参数 autoincrement - 指定id应自动递增，SQLite上的自动递增会引入额外的资源使用，通常可以通过基于此索引来避免
 */
@Id["autoincrement" = Bool]
```
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示实体类在数据库表的主键ID, 默认不会自增。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 */
@Id
```
#### 使用方法
```cangjie
@Entity
public class Demo {
    @Id["autoincrement" = true]
    public var id: Int64
}
```
### 1.7 @ToOne
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示实体类和其他实体类的一对一关系。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 * 参数 joinProperty - 当前实体内持有相关实体主键的字段的名称（区分大小写）。
 */
@ToOne["joinProperty" = String]
```
#### 使用方法
```cangjie
@Entity
public class User {
    @Id["autoincrement" = true]
    public var id: Int64
    public var name: String
    public var number: Int64
    @ToOne[joinProperty = "number"]
    public var userSon: UserSon
}
@Entity
public class UserSon {
    @Id["autoincrement" = true]
    public var id: Int64
    public var name: String
}
```
### 1.8 @ToMany
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示实体类和其他实体类的一对多关系。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 * 参数 referencedJoinProperty - 目标实体中包含源（当前）实体id的属性名称, 该参数不能和joinProperties同时存在。
 * 参数 joinProperties - 表示多个一对多映射关系, 该参数不能和referencedJoinProperty同时存在。
 *                    - 参数 name - 当前实体类的属性名称
 *                    - 参数 referencedName - 目标实体中包含源（当前）实体id的属性名称
 * 该注解的各个参数之间没有顺序
 */
@ToMany[
    "referencedJoinProperty" = String,
    "joinProperties" = [
            {"name" = String, "referencedName"=String}
        ]          
    ]
```
#### 使用方法
```cangjie
@Entity
public class Student {
    @Id
    public var id: String
    public var name: String
    @ToMany["referencedJoinProperty" = "studentId"]
    var score: Array<Score>
}
@Entity
public class Score {
    @Id
    var id: String
    var score: Int64
    var typ :String
    var studentId: String
}
```
### 1.9 @Property
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示实体类的字段在数据库列的名称（字符串不能包含空格）。
 * 成员变量支持 Int64/Int32/Int16/Int8/Float64/String/Array<Byte>/DateTime/Bool
 * 参数 nameInDb - 此属性的数据库列的名称。默认值为字段名。
 */
@Property["nameInDb" = String]
```
#### 使用方法
```cangjie
@Entity
public class Demo {
    @Id["autoincrement" = true]
    @Property["nameInDb" = "NUMBER_ID"]
    public var id: Int64
}
```

### 1.10 @OrderBy
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示指定@ToMany关系的相关集合的排序，例如：@OrderBy["value" = "name asc,age DESC"]列表集合。
 * 参数 value - 逗号分隔的属性列表（必须和实体类成员变量名一致），请在属性名称后添加ASC或DESC（不区分大小写），例如：“propertyA DESC，propertyB ASC”。
 */
@OrderBy["value" = String]
```
#### 使用方法
```cangjie
@Entity
public class Student {
    @Id
    public var id: String
    public var name: String

    @ToMany["referencedJoinProperty" = "studentId"]
    @OrderBy["id desc"]
    var score: Array<Score>
}
@Entity
public class Score {
    @Id
    var id: String
    var score: Int64
    var typ :String
    var studentId: String
}
```
### 1.11 @JoinEntity
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示定义与联接表的*-to-*关系。
 * 参数 entity - 对应带有@Entity修饰实体类的类名
 * 参数 sourceProperty - 包含源（当前）实体id的联接实体类的属性名称。
 * 参数 targetProperty - 包含目标实体id的联接实体类的属性名称。
 * 该注解的各个参数之间没有顺序
 */
@JoinEntity[
    "entity" = String
    "sourceProperty" = String
    "targetProperty" = String
    ]
```
#### 使用方法
```cangjie
@Entity
public class ToManyEntity {
    @Id
    public var id: Int64
    public var name: String
    @ToMany
    @JoinEntity["entity" = "JoinManyToDateEntity", "sourceProperty" = "idToMany", "targetProperty" = "idDate"]
    public var dateEntityList: Array<DateEntity>
}
@Entity
public class DateEntity {
    @Id
    private var id: Int64
    private var date: Int64
}
@Entity
public class JoinManyToDateEntity {
    @Id["autoincrement" = true]
    private var id: Int64
    private var idToMany: Int64
    private var idDate: Int64
}
```

### 1.12 @PropertyConverter 和 @Convert
```cangjie
/**
 * 修饰带有@Entity修饰的实体类的成员变量, 表示实体变量类型和数据库字段类型可以进行转换.
 * 参数 converter - 表示转换的函数. 见 @PropertyConverter 宏定义和 class PropertyConverter<P, D> 抽象类定义
 * 参数 columnType - 表示转换后数据库字段类型, 支持Int64/Float64/String/Array<Byte>/DateTime/Bool
 * 该注解的各个参数之间没有顺序
 */
@Convert[
    "converter" = String
    "columnType" = String
    ]

/**
 * 修饰带有@Entity修饰的实体类的静态成员函数, 需要和@Convert.converter属性对应, 
 * 表示需要返回实现的转换器子类, 该注解对静态成员函数进行检查, 静态函数返回类型是抽象类必须是IPropertyConverter<P,D>类型。
 */
@PropertyConverter
```
#### 使用方法
```cangjie
public class MyPropertyConverter <: IPropertyConverter<ArrayList<String>, String> {
    protected func convertToEntityProperty(databaseValue: String): ArrayList<String>  {
        return ArrayList<String>(databaseValue.split(","))
    }
    protected func convertToDatabaseValue(entityProperty: ArrayList<String>): String {
        return "test"
    }
}   

@Entity
public class Demo {
    @Id["autoincrement"= true]
    public var id: Int64
    @Convert["converter" = "testfunc", "columnType" = "String"] // D 类型
    public var sss: ArrayList<String> // P 类型
    @PropertyConverter
    public static func testfunc(): IPropertyConverter<ArrayList<String>, String> { // IPropertyConverter<P, D>
        return MyPropertyConverter()
    }
}
```

## 2. dataORM4cj 核心接口
### 2.1 AbstractDao<T, K>
```cangjie
/*
 * 类名 - AbstractDao<T, K>
 * 功能 - 实体类对应的Dao抽象类
 */
public abstract class AbstractDao<T, K> <: IAbstractDao where K <: Equatable < K > & Hashable & ToString, T <: Object{

    /*
     * 构造函数, 此函数由注解生成器自动调用
     * 参数 config - DaoConfig
     */
    public init (config: DaoConfig < K >)

    /*
     * 构造函数 , 此函数由注解生成器自动调用
     * 参数 config - DaoConfig
     * 参数 daoSession - daoSession
     */
    public init (config: DaoConfig < K >,daoSession:?AbstractDaoSession)

    /*
     * 功能 - 获取当前Dao的DaoSession对象
     * 
     * 返回值 ?AbstractDaoSession - AbstractDaoSession
     */
    public func getSession () : ?AbstractDaoSession

    /*
     * 功能 - 获取对应表的表名
     * 
     * 返回值 String - 表名
     */
    public override open func getTablename () : String

    /*
     * 功能 - 获取实体类字段的属性数组
     * 
     * 返回值 Array < Property > - Array < Property >
     */
    public func getProperties () : Array < Property >

    /*
     * 功能 - PkProperty, 单个属性主键或空（如果没有主键或多属性主键）
     * 
     * 返回值 ?Property - Property
     */
    public func getPkProperty () : ?Property

    /*
     * 功能 - 获取对应表的字段数组
     * 
     * 返回值 Array < String > - Array < String >
     */
    public func getAllColumns () : Array < String >

    /*
     * 功能 - 获取主键的数组
     * 
     * 返回值 Array < String > - Array < String >
     */
    public func getPkColumns () : Array < String >

    /*
     * 功能 - 获取不是主键的字段数组
     * 
     * 返回值 Array < String > - Array < String >
     */
    public func getNonPkColumns () : Array < String >

    /*
     * 功能 - 加载匹配Key的实体类
     * 
     * 参数 key - key
     * 返回值 ?EntityType - 实体类, 没有返回None
     */
    public override func load (key:?Any) : ?EntityType

    /*
     * 功能 - 按照表的行号进行加载实体类
     * 
     * 参数 rowId - 行号
     * 返回值 ?EntityType - 实体类, 没有返回none
     */
    public func loadByRowId (rowId: Int64) : ?EntityType

    /*
     * 功能 - 加载所有实体类
     * 
     * 返回值 Array<?EntityType> - 实体类数组, 使用时需转换Object类到实体类
     */
    public func loadAll () : Array<?EntityType>

    /*
     * 功能 - 清除对应实体类的缓存
     * 
     * 参数 entity - 实体类
     * 返回值 Bool - 是否清除成功
     */
    public func detach (entity: T) : Bool

    /*
     * 功能 - 清除所有的缓存
     * 
     * 返回值 Unit - Unit
     */
    public func detachAll () : Unit

    /*
     * 功能 - 插入实体类
     * 
     * 参数 entity - 实体类
     * 返回值 Int64 - 返回影响的行数
     */
    public func insert (entity: EntityType) : Int64

    /*
     * 功能 - 插入而不设置主键
     * 
     * 参数 entity - 实体类
     * 返回值 Int64 - 返回影响的行数
     */
    public func insertWithoutSettingPk (entity: EntityType) : Int64

    /*
     * 功能 - 插入或者替换
     * 
     * 参数 entity - 实体类
     * 返回值 Int64 - 返回影响的行数
     */
    public func insertOrReplace (entity: EntityType) : Int64

    /*
     * 功能 - 保存实体类
     * 
     * 参数 entity - 实体类
     * 返回值 Unit - Unit
     */
    public func save (entity: EntityType) : Unit

    /*
     * 功能 - 使用sql语句查询
     * 
     * 参数 whereStr - sql语句
     * 参数 selectionArg - 占位符, 该值与sql参数语句中的占位符相对应。
     * 返回值 Array<?EntityType> - 返回实体类数组, 使用时需要将Object转换对应的实体类型
     */
    public func queryRaw (whereStr: String,selectionArg: Array < ValueType >) : Array<?EntityType>

    /*
     * 功能 - 删除表中所有数据,使用时请谨慎操作
     * 
     * 返回值 Unit - Unit
     */
    public func deleteAll () : Unit

    /*
     * 功能 - 删除对应的实体类
     * 
     * 参数 entity - 实体类
     * 返回值 Unit - Unit
     */
    public override func delete (entity:?EntityType) : Unit

    /*
     * 功能 - 删除对应Key的数据
     * 
     * 参数 key - key
     * 返回值 Unit - unit
     */
    public func deleteByKey (key: K) : Unit

    /*
     * 功能 - 刷新数据
     * 
     * 参数 entity - 实体类
     * 返回值 Unit - Unit
     */
    public open func refresh (entity: EntityType) : Unit

    /*
     * 功能 - 更新实体类到数据表中
     * 
     * 参数 entity - 实体类
     * 返回值 Unit - Unit
     */
    public func update (entity: EntityType) : Unit

    /*
     * 功能 - 更新数据表
     * 
     * 参数 entity - 实体类
     * 参数 builder - QueryBuilder
     * 返回值 Unit - Unit
     */
    public func update (entity: EntityType,builder: QueryBuilder) : Unit

    /*
     * 功能 - 获取queryBuilder
     * 
     * 返回值 QueryBuilder - queryBuilder
     */
    public open func queryBuilder () : QueryBuilder

    /*
     * 功能 - 返回数据表中的行数
     * 
     * 返回值 Int64 - 行数
     */
    public func count () : Int64
}
```

### 2.2 AbstractDaoMaster
```cangjie
/*
 * 类名 - AbstractDaoMaster
 * 功能 - 控制DaoSession, 并创建和删除实体类对应表的功能, 具体功能实现在实现类中体现
 */
public abstract class AbstractDaoMaster  {

    /*
     * 构造函数 
     * 参数 db - GreenDaoDatabase
     * 参数 schemaVersion - schemaVersion版本
     */
    public init (db: GreenDaoDatabase,schemaVersion: Int64)

    /*
     * 功能 - 获取当前schema的版本
     * 
     * 返回值 Int64 - 版本号
     */
    public open func getSchemaVersion () : Int64

    /*
     * 功能 - 创建DaoSession
     * 
     * 返回值 AbstractDaoSession - AbstractDaoSession
     */
    public func newSession () : AbstractDaoSession

    /*
     * 功能 - 创建DaoSession
     * 
     * 参数 types - 主键范围类型
     * 返回值 AbstractDaoSession - AbstractDaoSession
     */
    public func newSession (types: IdentityScopeType) : AbstractDaoSession
}
```

### 2.3 AbstractDaoSession
```cangjie
/*
 * 类名 - AbstractDaoSession
 * 功能 - 抽象的Dao会话对象, 用于子类继承.
 */
public open class AbstractDaoSession  {

    /*
     * 功能 - 插入 实体类对象
     * 
     * 参数 entity - 实体类对象
     * 返回值 Int64 - 变更的行数
     */
    public func insert (entity: EntityType) : Int64

    /*
     * 功能 - 插入或替换 实体类对象
     * 
     * 参数 entity - 实体类对象
     * 返回值 Int64 - 变更的行数
     */
    public func insertOrReplace (entity: EntityType) : Int64

    /*
     * 功能 - 刷新实体类数据
     * 
     * 参数 entity - 被刷新的实体类
     * 返回值 Unit - unit
     */
    public func refresh (entity: EntityType) : Unit

    /*
     * 功能 - 更改实体类数据到表中
     * 
     * 参数 entity - 实体类
     * 返回值 Unit - Unit
     */
    public func update (entity: EntityType) : Unit

    /*
     * 功能 - 删除 实体类在表中的数据
     * 
     * 参数 entity - 实体类
     * 返回值 Unit - Unit
     */
    public func delete (entity: EntityType) : Unit

    /*
     * 功能 - 删除所有对应的实体类的表数据
     * 
     * 参数 entity - 实体类
     * 返回值 Unit - Unit
     */
    public func deleteAll (clazz: EntityType) : Unit

    /*
     * 功能 - 加载clazz类型匹配的Key的实体类
     * 
     * 参数 clazz - clazz类型
     * 参数 key - 主键key
     * 返回值 ?EntityType - 实体类, 没有返回None
     */
    public func load (clazz: EntityType,key: Any) : ?EntityType

    /*
     * 功能 - 加载clazz类型的所有实体类
     * 
     * 参数 clazz - clazz类型
     * 返回值 Array<?EntityType> - Array<?EntityType>类型, 使用时需要将Object转换对应的实体类型
     */
    public func ListloadAll (clazz: EntityType) : Array<?EntityType>

    /*
     * 功能 - 使用自定义sql语句查询, 支持占位符
     * 
     * 参数 clazz - clazz类型
     * 参数 whereStr - sql语句
     * 参数 selectionArgs - 占位符,SQL语句中参数的值。该值与sql参数语句中的占位符相对应
     * 返回值 Array<?EntityType> - 返回Object数组, 使用时需要将Object转换对应的实体类型
     */
    public func queryRaw (clazz: EntityType,whereStr: String,selectionArgs: Array < ValueType >) : Array<?EntityType>

    /*
     * 功能 - 获取对应实体类的QueryBuilder对象.
     * 
     * 参数 entityClass - clazz类型
     * 返回值 QueryBuilder - QueryBuilder
     */
    public func queryBuilder (entityClass: EntityType): QueryBuilder

    /*
     * 功能 - 获取对应实体类的IAbstractDao对象
     * 
     * 参数 entityClass - clazz类型
     * 返回值 IAbstractDao - IAbstractDao对象,使用时需转换成对应的实体类Dao对象
     */
    public func getDao (entityClass: EntityType) : IAbstractDao

    /*
     * 功能 - 获取GreenDaoDatabase对象
     * 
     * 返回值 GreenDaoDatabase - GreenDaoDatabase
     */
    public func getDatabase () : GreenDaoDatabase

    /*
     * 功能 - 获取所有的IAbstractDao集合
     * 
     * 返回值 Collection < IAbstractDao > - IAbstractDao集合
     */
    public func getAllDaos () : Collection < IAbstractDao >
}

```

### 2.4 DaoException
```cangjie
/*
 * 类名 - DaoException
 * 功能 - 异常
 */
public class DaoException <: Exception {

    /*
     * 构造函数 
     */
    public init ()

    /*
     * 构造函数 
     * 参数 msg - String
     */
    public init (msg: String)
}
```

### 2.5 EntityType
```cangjie
/*
 * 类名 - EntityType
 * 功能 - 实体类超类, 用于判断对象相等和获取哈希值以及打印
 */
public abstract class EntityType <: Object & Equatable & Hashable & ToString {
    /*
     * 功能 - 判断对象是否相等
     * 
     * 参数 that - EntityType对象
     * 返回值 Bool - 相等返回true, 否则返回false
     */
    public open operator func == (that: EntityType) : Bool

    /*
     * 功能 - 判断对象是否不相等
     * 
     * 参数 that - EntityType对象
     * 返回值 Bool - 相等返回true, 否则返回false
     */
    public open operator func != (that: EntityType) : Bool

    /*
     * 功能 - 返回哈希值
     * 
     * 返回值 Int64 - 哈希值
     */
    public func hashCode () : Int64

    /*
     * 功能 - 打印方法
     * 
     * 返回值 String - 字符串 
     */
    public open func toString () : String
    
    /*
     * 功能 - 给当前实体类设置AbstractDaoSession
     * 
     * 参数 daoSession - AbstractDaoSession
     * 返回值 Unit - Unit
     */
    public open func __setDaoSession (daoSession:?AbstractDaoSession) : Unit
}
```

### 2.6 Property
```cangjie
/*
 * 类名 - Property
 * 功能 - 实体类的字段属性类
 */
public class Property <: Equatable {
    // 序数
    public let ordinal: Int64

    // 类型
    public let TYPE: Any

    // 表名
    public let name: String

    // 主键
    public let primaryKey: Bool

    // 字段名
    public let columnName: String

    /*
     * 功能 - 判断对象是否相等
     * 
     * 参数 that - Property对象
     * 返回值 Bool - 相等返回true, 否则返回false
     */
    public operator func == (that: Property) : Bool

    /*
     * 功能 - 判断对象是否不相等
     * 
     * 参数 that - Property对象
     * 返回值 Bool - 不相等返回true, 否则返回false
     */
    public operator func != (that: Property) : Bool
    
    /*
     * 功能 - 拼接sql, 配置谓词以匹配数据表的field列中值为value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func eq (value: Any) : WhereCondition

    /*
     * 功能 - 拼接sql, 以匹配数据表的field列中值为value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func eqSql (value: Any) : WhereCondition

    /*
     * 功能 - 拼接sql, 配置谓词以匹配数据表的field列中值不为value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func notEq (value: Any) : WhereCondition

    /*
     * 功能 - 拼接sql, 配置以匹配数据表的field列中值不为value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func notEqSql (value: Any) : WhereCondition

    /*
     * 功能 - 拼接sql, 配置谓词以匹配数据表的field列中值类似于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func like (value: String) : WhereCondition

    /*
     * 功能 - 拼接sql, 配置以匹配数据表的field列中值类似于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func likeSql (value: String) : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值在给定范围内的字段（包含范围边界）。
     * 
     * 参数 value1 - 边界值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 参数 value2 - 边界值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func between (value1: Any, value2: Any) : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值在给定范围内的字段（包含范围边界）。
     * 
     * 参数 value1 - 边界值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 参数 value2 - 边界值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func betweenSql (value1: Any, value2: Any) : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值在给定范围内的字段。
     * 
     * 参数 inValues - 数据范围, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func inData (inValues: Array < Any >) : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值在给定范围内的字段。
     * 
     * 参数 inValues - 数据范围, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func inDataSql (inValues: Array < Any >) : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值不在给定范围内的字段。
     * 
     * 参数 notInValues - 数据范围, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func notIn (notInValues: Array < Any >) : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值不在给定范围内的字段。
     * 
     * 参数 notInValues - 数据范围, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func notInSql (notInValues: Array < Any >) : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值大于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func gt (value: Any) : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值大于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func gtSql (value: Any) : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值小于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func lt (value: Any) : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值小于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func ltSql (value: Any) : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值大于或者等于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func ge (value: Any) : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值大于或者等于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func geSql (value: Any) : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值小于或者等于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func le (value: Any) : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值小于或者等于value的字段。
     * 
     * 参数 value - 匹配的值, 只支持(String, Int8, Int16, Int32, Int64, Float64, Float32, Bool, Array<Byte>, DateTime)类型
     * 返回值 WhereCondition - where 条件类
     */
    public func leSql (value: Any) : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值为null的字段。
     * 
     * 返回值 WhereCondition - where 条件类
     */
    public func isNull () : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值为null的字段。
     * 
     * 返回值 WhereCondition - where 条件类
     */
    public func isNullSql () : WhereCondition

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值不为null的字段。
     * 
     * 返回值 WhereCondition - where 条件类
     */
    public func isNotNull () : WhereCondition

    /*
     * 功能 - 配置以匹配数据表的field列中值不为null的字段。
     * 
     * 返回值 WhereCondition - where 条件类
     */
    public func isNotNullSql () : WhereCondition
}
```

### 2.7 IPropertyConverter<P, D>
```cangjie
/*
 * 类名 - IPropertyConverter<P, D>
 * 功能 - 用于转换器的类
 */
public abstract class IPropertyConverter<P, D>  {

    /*
     * 功能 - D类型转换为P类型
     * 
     * 参数 dataObject - D类型
     * 返回值 P - p类型
     */
    public func DtoP (dataObject: D) : P

    /*
     * 功能 - P类型转换D类型
     * 
     * 参数 entityProperty - P类型 
     * 返回值 D - D类型
     */
    public func PtoD (entityProperty: P) : D

    /*
     * 功能 - D类型转换为P类型,此函数需要继承实现
     * 
     * 参数 databaseValue - D类型
     * 返回值 P - p类型
     */
    protected func convertToEntityProperty(databaseValue: D): P

    /*
     * 功能 - P类型转换为D类型,此函数需要继承实现
     * 
     * 参数 entityProperty - p类型
     * 返回值 D -  D类型
     */
    protected func convertToDatabaseValue(entityProperty: P): D
}
```

## 3. dataORM4cj 其他接口
### 3.1 AbstractCondition
```cangjie
/*
 * 类名 - AbstractCondition
 * 功能 - 抽象的条件类
 */
public abstract class AbstractCondition <: WhereCondition {
    public var hasSingleValue: Bool

    public var value:?Any

    public var values: Array < Any >


    /*
     * 构造函数 
     * 参数 value - 单个条件值
     * 参数 values - 多个条件值
     */
    public init init (value:?Any, values: Array < Any >)

    /*
     * 功能 - 拼接条件函数
     * 
     * 参数 builder - sql拼接器 
     * 参数 tableAlias - 字段别名
     * 返回值 Unit - unit
     */
    public open func appendTo (builder: StringBuilder, tableAlias: String) : Unit

    /*
     * 功能 - 拼接条件函数
     * 
     * 参数 valuesTarget - 需要拼接的多个条件值
     * 返回值 Unit - unit
     */
    public open func appendValuesTo (valuesTarget: ArrayList < Any >) : Unit
}

```

### 3.2 PropertyCondition
```cangjie

/*
 * 类名 - PropertyCondition
 * 功能 - 属性条件类型
 */
public class PropertyCondition <: AbstractCondition {
    public var property: Property

    public var op: String

    public var predicates:?RdbPredicatesWithLock = None


    /*
     * 功能 - 设置谓词
     * 
     * 参数 predicates - 谓词类 
     * 返回值 PropertyCondition -  PropertyCondition
     */
    public func setPredicates (predicates: RdbPredicatesWithLock) : PropertyCondition

    /*
     * 功能 - 获取谓词类
     * 
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock
     */
    public func getPredicates () : RdbPredicatesWithLock

    /*
     * 构造函数 
     * 参数 property - 属性类
     * 参数 op - 条件关键字
     */
    public init init (property: Property, op: String)

    /*
     * 构造函数 
     * 参数 property - 属性类
     * 参数 op - 条件关键字
     * 参数 value - 单个条件值
     */
    public init init (property: Property, op: String, value: Any)

    /*
     * 构造函数 
     * 参数 property - 属性类
     * 参数 op - 条件关键字
     * 参数 values - 多个条件值
     */
    public init init (property: Property, op: String, values: Array < Any >)

    /*
     * 构造函数 
     * 参数 property - 属性类
     * 参数 op - 条件关键字
     * 参数 value - 单个条件值
     * 参数 values - 多个条件值
     */
    public init init (property: Property, op: String, value: Any, values: Array < Any >)

    /*
     * 功能 - 拼接sql函数
     * 
     * 参数 builder - sql构建器
     * 参数 tableAlias - 数据库表字段名
     * 返回值 - Unit
     */
    public func appendTo (builder: StringBuilder, tableAlias: String)
}
```

### 3.3 RdbPredicatesWithLock
```cangjie
/*
 * 类名 - RdbPredicatesWithLock
 * 功能 - 谓词类
 */
public class RdbPredicatesWithLock <: GreenDaoEqualTo {

    /*
     * 构造函数 
     * 参数 name - 表名
     */
    public init init (name: String)

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值为value的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func equalTo (field: String, value: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值不为value的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func notEqualTo (field: String, value: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 向谓词添加左括号。
     * 
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func beginWrap () : RdbPredicatesWithLock

    /*
     * 功能 - 向谓词添加右括号。
     * 
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func endWrap () : RdbPredicatesWithLock

    /*
     * 功能 - 将或条件添加到谓词中。
     * 
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func or () : RdbPredicatesWithLock

    /*
     * 功能 - 向谓词添加和条件。
     * 
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func and () : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中包含value的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func contains (field: String, value: String) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中以value开头的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func beginsWith (field: String, value: String) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中以value结尾的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func endsWith (field: String, value: String) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值为null的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func isNull (field: String) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值不为null的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func isNotNull (field: String) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值类似于value的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func like (field: String, value: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词匹配数据字段为string的指定字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。支持通配符，*表示0个、1个或多个数字或字符，?表示1个数字或字符。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func glob (field: String, value: String) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值在给定范围内的字段（包含范围边界）。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 low - 指示与谓词匹配的最小值。
     * 参数 high - 指示与谓词匹配的最大值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func between (field: String, low: Any, high: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值超出给定范围的字段（不包含范围边界）。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 low - 指示与谓词匹配的最小值。
     * 参数 high - 指示与谓词匹配的最大值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func notBetween (field: String, low: Any, high: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值大于value的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func greaterThan (field: String, value: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值小于value的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func lessThan (field: String, value: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值大于等于value的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func greaterThanOrEqualTo (field: String, value: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值小于等于value的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func lessThanOrEqualTo (field: String, value: Any) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值按升序排序的列。
     * 
     * 参数 field - 数据库表中的列名。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func orderByAsc (field: String) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值按降序排序的列。
     * 
     * 参数 field - 数据库表中的列名。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func orderByDesc (field: String) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以过滤重复记录并仅保留其中一个。
     * 
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func distinct () : RdbPredicatesWithLock

    /*
     * 功能 - 设置最大数据记录数的谓词。
     * 
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func limitAs (value: Int32) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以指定返回结果的起始位置, 此方法必须与limitAs一起使用。
     * 
     * 参数 rowOffset - 返回结果的起始位置，取值为正整数。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func offsetAs (rowOffset: Int32) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词按指定列分组查询结果。
     * 
     * 参数 fields - 指定分组依赖的列名。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func groupBy (fields: Array < String >) : RdbPredicatesWithLock

    /*
     * 功能 - 配置谓词以匹配数据表的field列中值在给定范围内的字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func `in` (field: String, value: Array < Any >) : RdbPredicatesWithLock

    /*
     * 功能 - 将谓词配置为匹配数据字段为ValueType且值超出给定范围的指定字段。
     * 
     * 参数 field - 数据库表中的列名。
     * 参数 value - 指示要与谓词匹配的值。
     * 返回值 RdbPredicatesWithLock - RdbPredicatesWithLock谓词类
     */
    public func notIn (field: String, value: Array < Any >) : RdbPredicatesWithLock
}

```

### 3.4 DaoConfig
```cangjie
/*
 * 类名 - DaoConfig
 * 功能 - dao配置类
 */
public class DaoConfig<K> <: IDaoConfig where K <: Equatable < K > & Hashable{
    public let tablename: String

    public let properties: Array < Property >

    public let allColumns: Array < String >

    public let pkColumns: Array < String >

    public let nonPkColumns: Array < String >

    public let pkProperty:?Property

    public let keyIsNumeric: Bool

    public let statements: TableStatements


    /*
     * 功能 - 初始化IdentityScopeType
     * 
     * 参数 t - IdentityScopeType类型
     * 返回值 Unit - unit
     */
    public func initIdentityScope (t: IdentityScopeType) : Unit

    /*
     * 构造函数 
     * 参数 master - AbstractDaoMaster类
     * 参数 tablename - 表名
     * 参数 pts - 类字段和属性类集合
     */
    public init init (master: AbstractDaoMaster, tablename: String, pts: HashMap < String, Property >)

    /*
     * 构造函数 
     * 参数 source - DaoConfig类
     */
    public init init (source: DaoConfig < K >)

    /*
     * 功能 - 克隆方法
     * 
     * 返回值 DaoConfig < K > - DaoConfig()
     */
    public func clone () : DaoConfig < K >

    /*
     * 功能 - 获取IdentityScope
     * 
     * 返回值 ?IdentityScope - IdentityScope
     */
    public func getIdentityScope () : ?IdentityScope

    /*
     * 功能 - 清空IdentityScope
     * 
     * 返回值 Unit - unit
     */
    override public func clearIdentityScope () : Unit

    /*
     * 功能 - 设置IdentityScope
     * 
     * 参数 identityScope - IdentityScope
     * 返回值 Unit - unit
     */
    public func setIdentityScope (identityScope: IdentityScope) : Unit
}
```

### 3.5 InternalQueryDaoAccess
```cangjie
/*
 * 类名 - InternalQueryDaoAccess
 * 功能 - 内部查询dao访问
 */
public class InternalQueryDaoAccess<T>  {

    /*
     * 功能 - 加载当前结果集
     * 
     * 参数 cursor - 结果集
     * 参数 offset - 偏移量
     * 参数 lock - 锁
     * 返回值 ?T - T类型
     */
    public func loadCurrent (cursor: ResultSet, offset: Int64, lock: Bool) : ?T

    /*
     * 功能 - 加载当前结果集并关闭结果集
     * 
     * 参数 cursor - 结果集
     * 返回值 Array < T > - T类型集合
     */
    public func loadAllAndCloseCursor (cursor: ResultSet) : Array < T >

    /*
     * 功能 - 加载唯一的结果集
     * 
     * 参数 cursor - 结果集
     * 返回值 ?T - T类型
     */
    public func loadUniqueAndCloseCursor (cursor: ResultSet) : ?T

    /*
     * 功能 - 获取TableStatements
     * 
     * 返回值 TableStatements - TableStatements
     */
    public func getStatements () : TableStatements

    /*
     * 功能 - 获取TableStatements
     * 
     * 参数 dao - 
     * 返回值 TableStatements - TableStatements
     */
    public static func getStatementsWithStatic (dao: IAbstractDao) : TableStatements
}

```

### 3.6 SqlUtils
```cangjie
/*
 * 类名 - SqlUtils
 * 功能 - sql语句工具
 */
public class SqlUtils  {

    /*
     * 功能 - 拼接属性
     * 
     * 参数 builder - 拼接器 
     * 参数 tablePrefix - 表前缀
     * 参数 property - 属性值类
     * 返回值 StringBuilder - 拼接器 
     */
    public static func appendProperty (builder: StringBuilder, tablePrefix:?String, property: Property) : StringBuilder

    /*
     * 功能 - 拼接字段
     * 
     * 参数 builder - 拼接器 
     * 参数 column - 字段名
     * 返回值 StringBuilder - 拼接器 
     */
    public static func appendColumn (builder: StringBuilder, column: String) : StringBuilder

    /*
     * 功能 - 拼接字段
     * 
     * 参数 builder -  拼接器 
     * 参数 tableAlias - 表别名
     * 参数 column - 字段名
     * 返回值 StringBuilder - 拼接器 
     */
    public static func appendColumn (builder: StringBuilder, tableAlias: String, column: String) : StringBuilder

    /*
     * 功能 - 拼接多个字段
     * 
     * 参数 builder - 拼接器
     * 参数 tableAlias - 表别名
     * 参数 columns - 多个字段名
     * 返回值 StringBuilder - 拼接器
     */
    public static func appendColumns (builder: StringBuilder, tableAlias: String, columns: Array < String >) : StringBuilder

    /*
     * 功能 - 拼接多个字段
     * 
     * 参数 builder - 拼接器
     * 参数 columns - 多个字段名
     * 返回值 StringBuilder - 拼接器
     */
    public static func appendColumns (builder: StringBuilder, columns: Array < String >) : StringBuilder

    /*
     * 功能 - 附加占位符
     * 
     * 参数 builder - 拼接器
     * 参数 count - 计数
     * 返回值 StringBuilder - 拼接器
     */
    public static func appendPlaceholders (builder: StringBuilder, count: Int64) : StringBuilder

    /*
     * 功能 - 拼接列等于占位符
     * 
     * 参数 builder - 拼接器
     * 参数 columns - 多个列名
     * 返回值 StringBuilder - 拼接器
     */
    public static func appendColumnsEqualPlaceholders (builder: StringBuilder, columns: Array < String >): StringBuilder

    /*
     * 功能 - 拼接列等于值
     * 
     * 参数 builder - 拼接器
     * 参数 tableAlias - 表别名
     * 参数 columns - 多个列名
     * 返回值 StringBuilder - 拼接器
     */
    public static func appendColumnsEqValue (builder: StringBuilder, tableAlias: String, columns: Array < String >) : StringBuilder

    /*
     * 功能 - 构建插入sql语句
     * 
     * 参数 insertInto - 插入语句
     * 参数 tablename - 表名
     * 参数 columns - 多个列名
     * 返回值 String - sql字符串
     */
    public static func createSqlInsert (insertInto: String, tablename: String, columns: Array < String >) : String

    /*
     * 功能 - 构建查询语句
     * 
     * 参数 tablename - 表名
     * 参数 tableAlias - 表别名
     * 参数 columns - 多个列名
     * 参数 distinct - 是否去重
     * 返回值 String - sql字符串
     */
    public static func createSqlSelect (tablename: String, tableAlias:?String, columns: Array < String >, distinct: Bool) : String

    /*
     * 功能 - 构建查询sql语句count(*)
     * 
     * 参数 tablename - 表名
     * 参数 tableAliasOrNull -表别名是否存在 
     * 返回值 String - sql字符串
     */
    public static func createSqlSelectCountStar (tablename: String, tableAliasOrNull:?String) : String

    /*
     * 功能 - 创建删除sql语句
     * 
     * 参数 tablename - 表名
     * 参数 columns - 多个列名
     * 返回值 String - sql字符串
     */
    public static func createSqlDelete (tablename: String, columns: Array < String >) : String

    /*
     * 功能 - 创建更新sql语句
     * 
     * 参数 tablename - 表名
     * 参数 updateColumns - 更新字段名
     * 参数 whereColumns - where字段
     * 返回值 String - sql字符串
     */
    public static func createSqlUpdate (tablename: String, updateColumns: Array < String >, whereColumns: Array < String >) : String

    /*
     * 功能 - 创建count的sql语句
     * 
     * 参数 tablename - 表名
     * 返回值 String - sql字符串
     */
    public static func createSqlCount (tablename: String) : String

    /*
     * 功能 - 转义Blob参数
     * 
     * 参数 bytes - 数据
     * 返回值 String - sql字符串
     */
    public static func escapeBlobArgument (bytes: Array < Byte >) : String
}
```

### 3.7 TableStatements
```cangjie

/*
 * 类名 - TableStatements
 * 功能 - 处理sql的类
 */
public class TableStatements  {

    /*
     * 功能 - 获取插入sql表的结果
     * 
     * 参数 valueMap - valueMap
     * 返回值 Int64 - 返回影响的行数
     */
    public func getInsertStatement (valueMap: Map < String, ValueType >) : Int64

    /*
     * 功能 - 获取插入或替换的sql的结果
     * 
     * 参数 valueMap - valueMap
     * 返回值 Int64 - 返回影响的行数
     */
    public func getInsertOrReplaceStatement (valueMap: Map < String, ValueType >) : Int64

    /*
     * 功能 - 获取删除sql的结果
     * 
     * 参数 rdbPredicates - 谓词 
     * 返回值 Int64 - 返回影响的行数
     */
    public func getDeleteStatement (rdbPredicates: RdbPredicates) : Int64

    /*
     * 功能 - 获取更新sql的结果
     * 
     * 参数 valueMap - valueMap
     * 参数 rdbPredicates - 谓词
     * 返回值 Int64 - 返回影响的行数
     */
    public func getUpdateStatement (valueMap: Map < String, ValueType >, rdbPredicates: RdbPredicates) : Int64

    /*
     * 功能 - 获取count函数sql的结果
     * 
     * 返回值 String - 字符串
     */
    public func getCountStatement () : String

    /*
     * 功能 - 获取查询所有的sql
     * 
     * 返回值 ResultSet - 结果集
     */
    public func getSelectAll () : ResultSet

    /*
     * 功能 - 获取查询主键的结果集
     * 
     * 返回值 ResultSet - 结果集
     */
    public func getSelectKeys () : ResultSet

    /*
     * 功能 - 获取查询主键的结果集
     * 
     * 参数 condition - 条件
     * 返回值 ResultSet - 结果集
     */
    public func getSelectByKey (condition: Array < String >) : ResultSet

    /*
     * 功能 - 获取查询主对应行号的结果集
     * 
     * 返回值 ResultSet - 结果集
     */
    public func getSelectByRowId () : ResultSet

    /*
     * 功能 - 获取查询主键的结果集
     * 
     * 返回值 String - 字符串
     */
    public func getSelectByKey () : String
}
```
### 3.8 IAbstractQuery
```cangjie
/*
 * 类名 - IAbstractQuery
 * 功能 - 查询类的父类
 */
sealed abstract class IAbstractQuery {
    /*
     * 功能 - 设置参数
     * 
     * 参数 index - 位置
     * 参数 parameter - Any类型参数 
     * 返回值 AbstractQueryWithLimit < T > - IAbstractQuery 
     */
    public func setParameter(index: Int64 , parameter: Any): IAbstractQuery
    
    /*
     * 功能 - 设置参数
     * 
     * 参数 index - 位置
     * 参数 parameter - DateTime参数 
     * 返回值 AbstractQueryWithLimit < T > - IAbstractQuery 
     */
    public func setParameter(index: Int64, parameter: ?DateTime ): IAbstractQuery
    
    /*
     * 功能 - 设置参数
     * 
     * 参数 index - 位置
     * 参数 parameter - Bool参数 
     * 返回值 AbstractQueryWithLimit < T > - IAbstractQuery 
     */
    public func setParameter(index: Int64, parameter: ?Bool): IAbstractQuery
}

```
### 3.9 AbstractQueryWithLimit
```cangjie
/*
 * 类名 - AbstractQueryWithLimit
 * 功能 - 抽象的带有限定范围查询类
 */
public abstract class AbstractQueryWithLimit<T> <: AbstractQuery where T <: EntityType{

    /*
     * 功能 - 设置限定范围
     * 
     * 参数 limit - 范围
     * 返回值 Unit - Unit
     */
    public func setLimit (limit: Int64) : Unit

    /*
     * 功能 - 设置偏移量
     * 
     * 参数 offset - 偏移量
     * 返回值 Unit - Unit
     */
    public func setOffset (offset: Int64) : Unit
}
```

### 3.10 IQueryBuilder
```cangjie
/*
 * 接口名 - IQueryBuilder
 * 功能 - IQueryBuilder的接口
 */
sealed interface IQueryBuilder  {
    // 设置 sql 是否打印在hilog控制台中
    static mut prop LOG_SQL: Bool
    // 设置 sql中的值 是否打印在hilog控制台中
    static mut prop LOG_VALUES: Bool
}

/*
 * 类名 - QueryBuilder
 * 功能 - 查询构建器
 */
public class QueryBuilder<T> <: IQueryBuilder where T <: EntityType{

    /*
     * 功能 - 内部构建方法
     * 
     * 参数 dao - 实体dao类 
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public static func internalCreate (dao: IAbstractDao) : QueryBuilder < T >

    /*
     * 功能 - 去重操作
     * 
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func distinct () : QueryBuilder < T >

    /*
     * 功能 - 使用逻辑AND将给定条件添加到where子句中。要创建新条件，请使用生成的dao类中给出的属性。
     * 
     * 参数 cond - 条件
     * 参数 condMore - 条件集合 
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func `where` (cond: WhereCondition, condMore: Array < WhereCondition >) : QueryBuilder < T >

    /*
     * 功能 - 使用逻辑AND将给定条件添加到where子句中sql语句。要创建新条件，请使用生成的dao类中给出的属性。
     * 
     * 参数 cond - 条件
     * 参数 condMore - 条件集合
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func `whereSql` (cond: WhereCondition, condMore: Array < WhereCondition >) : QueryBuilder < T >

    /*
     * 功能 - 使用逻辑OR将给定条件添加到where子句中。要创建新条件，请使用生成的dao类中给出的属性。
     * 
     * 参数 cond1 - 条件句
     * 参数 cond2 - 条件句
     * 参数 condMore - 条件集合
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func whereOr (cond1: WhereCondition, cond2: WhereCondition, condMore: Array < WhereCondition >) : QueryBuilder < T >

    /*
     * 功能 - 逻辑OR
     * 
     * 参数 cond1 - 条件 
     * 参数 cond2 - 条件
     * 参数 condMore - 条件集合
     * 返回值 WhereCondition - where 条件类
     */
    public func or (cond1: WhereCondition, cond2: WhereCondition, condMore: Array < WhereCondition >) : WhereCondition

    /*
     * 功能 - 逻辑AND
     * 
     * 参数 cond1 - 条件 
     * 参数 cond2 - 条件
     * 参数 condMore - 条件集合
     * 返回值 WhereCondition - where 条件类
     */
    public func and (cond1: WhereCondition, cond2: WhereCondition, condMore: Array < WhereCondition >) : WhereCondition

    /*
     * 功能 - 逻辑OR的sql语句
     * 
     * 参数 cond1 - 条件 
     * 参数 cond2 - 条件
     * 参数 condMore - 条件集合
     * 返回值 WhereCondition - where 条件类
     */
    public func sqlOr (cond1: WhereCondition, cond2: WhereCondition, condMore: Array < WhereCondition >) : WhereCondition

    /*
     * 功能 - 逻辑AND的sql语句
     * 
     * 参数 cond1 - 条件 
     * 参数 cond2 - 条件
     * 参数 condMore - 条件集合
     * 返回值 WhereCondition - where 条件类
     */
    public func sqlAnd (cond1: WhereCondition, cond2: WhereCondition, condMore: Array < WhereCondition >) : WhereCondition

    /*
     * 功能 - join语句
     * 
     * 参数 destinationEntityClass - 目标实体类
     * 参数 destinationProperty - 目的属性
     * 返回值 Join < T, J > - Join类
     */
    public func join (destinationEntityClass: EntityType, destinationProperty: Property) : Join < T, J >

    /*
     * 功能 - join语句
     * 
     * 参数 sourceProperty - 源属性
     * 参数 destinationEntityClass - 目标实体类
     * 返回值 Join < T, J > - Join类
     */
    public func join (sourceProperty: Property, destinationEntityClass: EntityType) : Join < T, J >

    /*
     * 功能 - join语句
     * 
     * 参数 sourceProperty - 源属性
     * 参数 destinationEntityClass - 目标实体类
     * 参数 destinationProperty - 目的属性
     * 返回值 Join < T, J > - Join类
     */
    public func join (sourceProperty: Property, destinationEntityClass: EntityType, destinationProperty: Property) : Join < T, J >

    /*
     * 功能 - join语句
     * 
     * 参数 sourceJoin - join类
     * 参数 sourceProperty - 源属性
     * 参数 destinationEntityClass - 目标实体类
     * 参数 destinationProperty - 目的属性
     * 返回值 IJoin - Join类
     */
    public func join (sourceJoin: IJoin, sourceProperty: Property, destinationEntityClass: EntityType, destinationProperty: Property) : IJoin

    /*
     * 功能 - asc排序
     * 
     * 参数 properties - 属性集合
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func orderAsc (properties: Array < Property >) : QueryBuilder < T >

    /*
     * 功能 - desc排序
     * 
     * 参数 properties - 属性集合
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func orderDesc (properties: Array < Property >) : QueryBuilder < T >

    /*
     * 功能 - 自定排序
     * 
     * 参数 property - 属性 
     * 参数 customOrderForProperty -自定排序属性 
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func orderCustom (property: Property, customOrderForProperty: String) : QueryBuilder < T >

    /*
     * 功能 - 将给定的原始SQL字符串添加到ORDER BY部分。不要将其用于标准属性：首选orderAsc和orderDesc。
     * 
     * 参数 rawOrder - 原始sql
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func orderRaw (rawOrder: String) : QueryBuilder < T >

    /*
     * 功能 - 限制查询返回的结果数量。
     * 
     * 参数 lim - 限制值
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func limit (lim: Int64) : QueryBuilder < T >

    /*
     * 功能 - 结合limit（int）设置查询结果的偏移量。跳过第一个偏移结果，结果总数将受到限制。您不能无限制地使用偏移量。
     * 
     * 参数 offset - 偏移值
     * 返回值 QueryBuilder < T > - QueryBuilder
     */
    public func offset (offset: Int64) : QueryBuilder < T >

    /*
     * 功能 - 构建可重用的查询对象（与为每次执行创建QueryBuilder相比，可以更有效地执行查询对象。)
     * 
     * 返回值 Query < T > - Query查询类
     */
    public func build () : Query < T >

    /*
     * 功能 - 构建可重用的查询sql语句对象（与为每次执行创建QueryBuilder相比，可以更有效地执行查询对象。)
     * 
     * 返回值 Query < T > - Query查询类
     */
    public func buildSql () : Query < T >

    /*
     * 功能 - 构建可重用的查询对象。光标访问。（与为每次执行创建QueryBuilder相比，查询对象的执行效率更高。)
     * 
     * 返回值 CursorQuery < T > - CursorQuery类
     */
    public func buildCursor () : CursorQuery < T >

    /*
     * 功能 - 构建可重用的查询对象。光标访问。（与为每次执行创建QueryBuilder相比，查询对象的执行效率更高。)
     * 
     * 返回值 CursorQuery < T > - CursorQuery类
     */
    public func buildCursorSql () : CursorQuery < T >

    /*
     * 功能 - 构建可重用的查询对象以供删除（与每次执行创建QueryBuilder相比，查询对象的执行效率更高。
     * 
     * 返回值 DeleteQuery < T > - DeleteQuery类
     */
    public func buildDelete () : DeleteQuery < T >

    /*
     * 功能 - 构建可重用的查询对象以供删除（与每次执行创建QueryBuilder相比，查询对象的执行效率更高。
     * 
     * 返回值 DeleteQuery < T > - DeleteQuery类
     */
    public func buildDeleteSql () : DeleteQuery < T >

    /*
     * 功能 - 构建一个可重用的查询对象来计数行（与为每次执行创建QueryBuilder相比，可以更有效地执行查询对象。
     * 
     * 返回值 CountQuery < T > - CountQuery
     */
    public func buildCount () : CountQuery < T >

    /*
     * 功能 - 构建一个可重用的查询对象来计数行（与为每次执行创建QueryBuilder相比，可以更有效地执行查询对象。
     * 
     * 返回值 CountQuery < T > - CountQuery
     */
    public func buildCountSql () : CountQuery < T >

    /*
     * 功能 - build().list()的简写；有关详细信息，请参阅Query.list()。要多次执行查询，出于效率原因，您应该构建查询并保留query对象。
     * 
     * 返回值 Array < T > - Array < T >
     */
    public func list () : Array < T >

    /*
     * 功能 - build().unique()的简写；有关详细信息，请参阅Query.unique()。要多次执行查询，出于效率原因，您应该构建查询并保留query对象。
     * 
     * 返回值 ?T - ?T
     */
    public func unique () : ?T

    /*
     * 功能 - build().uniqueOrThrow()的简写；有关详细信息，请参阅Query.uniqueOrThrow()。要多次执行查询，出于效率原因，您应该构建查询并保留query对象。
     * 
     * 返回值 T - 
     */
    public func uniqueOrThrow () : T

    /*
     * 功能 - buildCount().count()的简写；有关详细信息，请参阅CountQuery.count()。要多次执行查询，出于效率原因，您应该构建查询并保留CountQuery对象。
     * 
     * 返回值 Int64 - 
     */
    public func count () : Int64
}

```

### 3.11 CountQuery
```cangjie
/*
 * 类名 - CountQuery
 * 功能 - 计数查询类
 */
public class CountQuery<T> <: AbstractQuery where T <: EntityType{

    /*
     * 功能 - 计数查询
     * 
     * 返回值 Int64 - 查询结果 
     */
    public func count () : Int64
}

```

### 3.12 CursorQuery
```cangjie
/*
 * 类名 - CursorQuery
 * 功能 - 光标查询类
 */
public class CursorQuery<T> <: AbstractQueryWithLimit where T <: EntityType{

    /*
     * 功能 - 光标查询
     * 
     * 返回值 ResultSet - 结果集
     */
    public func query () : ResultSet
}
```

### 3.13 DeleteQuery
```cangjie
/*
 * 类名 - DeleteQuery
 * 功能 - 删除查询类
 */
public class DeleteQuery<T> <: AbstractQuery where T <: EntityType{

    /*
     * 功能 - 执行删除操作
     * 
     * 返回值 Unit - Unit
     */
    public func executeDeleteWithoutDetachingEntities () : Unit

}
```

### 3.14 Join
```cangjie

/*
 * 类名 - Join
 * 功能 - join子句类
 */
public class Join<SRC, DST> <: IJoin {

    /*
     * 构造函数 
     * 参数 sourceTablePrefix - 前缀
     * 参数 sourceJoinProperty - 源属性
     * 参数 daoDestination - 目标实体类
     * 参数 destinationJoinProperty - 目的属性
     * 参数 joinTablePrefix - join表的前缀
     */
    public init init (sourceTablePrefix: String, sourceJoinProperty: Property, daoDestination: IAbstractDao, destinationJoinProperty: Property, joinTablePrefix: String)

    /*
     * 功能 - 使用逻辑AND将给定条件添加到where子句中
     * 
     * 参数 cond - 条件
     * 参数 condMore - 条件集合
     * 返回值 Join < SRC, DST > - Join < SRC, DST >
     */
    public func `where` (cond: WhereCondition, condMore: Array < WhereCondition >) : Join < SRC, DST >

    /*
     * 功能 - 使用逻辑OR将给定条件添加到where子句中
     * 
     * 参数 cond1 - 条件
     * 参数 cond2 - 条件
     * 参数 condMore - 条件集合
     * 返回值 Join < SRC, DST > - Join < SRC, DST >
     */
    public func whereOr (cond1: WhereCondition, cond2: WhereCondition, condMore: Array < WhereCondition >) : Join < SRC, DST >

    /*
     * 功能 - 逻辑OR
     * 
     * 参数 cond1 - 条件 
     * 参数 cond2 - 条件
     * 参数 condMore - 条件集合
     * 返回值 WhereCondition - where 条件类
     */
    public func or (cond1: WhereCondition, cond2: WhereCondition, condMore: Array < WhereCondition >) : WhereCondition

    /*
     * 功能 - 逻辑AND
     * 
     * 参数 cond1 - 条件 
     * 参数 cond2 - 条件
     * 参数 condMore - 条件集合
     * 返回值 WhereCondition - where 条件类
     */
    public func and (cond1: WhereCondition, cond2: WhereCondition, condMore: Array < WhereCondition >) : WhereCondition

    /*
     * 功能 - 获取表的前缀语句
     * 
     * 返回值 String - 表前缀
     */
    public func getTablePrefix () : String
}
```
### 3.15 Query
```cangjie
/*
 * 类名 - Query
 * 功能 - 查询类
 */
public class Query<T> <: AbstractQueryWithLimit where T <: EntityType{

    /*
     * 功能 - 执行查询并将结果作为包含加载到内存中的所有实体的列表返回。
     * 
     * 返回值 Array < E > - Array < E >
     */
    public func list () : Array < E >

    /*
     * 功能 - 执行查询并返回唯一结果或None。
     * 
     * 返回值 ?E - 结果类
     */
    public func unique () : ?E

    /*
     * 功能 - 执行查询并返回唯一结果（从不为空）。为空时抛空值异常
     * 
     * 返回值 T - 结果
     */
    public func uniqueOrThrow () : T
}
```

